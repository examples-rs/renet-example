mod cli;

use std::net::UdpSocket;
use std::time::{SystemTime, Instant};
use renet::{RenetClient, ConnectionConfig, DefaultChannel};
use renet::transport::{ClientAuthentication, NetcodeClientTransport};
use rand::Rng;
use clap::Parser;
use nanoid::nanoid;

use common::*;

fn main() {
    let args = cli::Cli::parse();

    let server_addr = args.server;

    let client_id: u64 = rand::thread_rng().gen();

    let mut client = RenetClient::new(ConnectionConfig::default());
    let socket = UdpSocket::bind("127.0.0.1:0").unwrap();
    let current_time = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap();
    let authentication = ClientAuthentication::Unsecure {
        protocol_id: PROTOCOL_ID,
        user_data: Some(Username(nanoid!()).to_netcode_user_data().unwrap()),
        server_addr,
        client_id,
    };

    let mut transport = NetcodeClientTransport::new(current_time, authentication, socket).unwrap();

    let pings = vec![
        "This is a ping.".to_string(),
        "Ping ping ping!".to_string(),
        "Beep boop bop.".to_string(),
    ];

    let mut last_updated = Instant::now();

    loop {
        let now = Instant::now();
        let duration = now - last_updated;
        last_updated = now;

        client.update(duration);
        transport.update(duration, &mut client).unwrap();

        if client.is_connected() {
            while let Some(message) = client.receive_message(DefaultChannel::ReliableOrdered) {
                let bytes = message.to_vec();
                let msg: ServerMessage = bincode::deserialize(&bytes).unwrap();

                if args.verbose {
                    println!("<SERVER> {:?}", msg);
                }
            }

            client.send_message(
                DefaultChannel::ReliableOrdered, // Channel.
                bincode::serialize(&ClientMessage::Ping(choice(&pings).into())).unwrap() // Message.
            );
        }

        // Send packets to server using the transport layer.
        transport.send_packets(&mut client).unwrap();
    }
}

fn choice<T>(vec: &Vec<T>) -> &T {
    return &vec[rand::thread_rng().gen_range(0..vec.len())];
}
