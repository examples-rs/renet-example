mod cli;

use std::net::UdpSocket;
use std::time::{SystemTime, Duration, Instant};
use renet::{RenetServer, ConnectionConfig, ServerEvent, DefaultChannel};
use renet::transport::{ServerAuthentication, ServerConfig, NetcodeServerTransport};
use clap::Parser;

use common::*;

fn main() {
    let args = cli::Cli::parse();

    let server_addr = args.ip;
    let max_clients = args.max;

    let mut server = RenetServer::new(ConnectionConfig::default());
    let socket: UdpSocket = UdpSocket::bind(server_addr).unwrap();
    let server_config = ServerConfig {
        current_time: SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap(),
        max_clients,
        protocol_id: PROTOCOL_ID,
        public_addresses: vec![server_addr],
        authentication: ServerAuthentication::Unsecure
    };

    let mut transport = NetcodeServerTransport::new(server_config, socket).unwrap();

    let mut last_updated = Instant::now();

    loop {
        let now = Instant::now();
        let duration = now - last_updated;
        last_updated = now;

        // Receive new messages, and update duration.
        server.update(duration);
        transport.update(duration, &mut server).unwrap();

        while let Some(event) = server.get_event() {
            match event {
                ServerEvent::ClientConnected { client_id } => {
                    let user_data = transport.user_data(client_id).unwrap();
                    let username = Username::from_user_data(&user_data);

                    println!("New client connected! (Username: {}, ID: {})", username.0, client_id);
                },
                ServerEvent::ClientDisconnected { client_id, reason } => {
                    println!("Client '{}' disconnected! (Reason: {})", client_id, reason);
                },
            };
        }

        for client_id in server.clients_id() {
            while let Some(message) = server.receive_message(client_id, DefaultChannel::ReliableOrdered) {
                let bytes = message.to_vec();
                let msg: ClientMessage = bincode::deserialize(&bytes).unwrap();

                if args.verbose {
                    println!("<{}> {:?}", client_id, msg);
                }

                // If a ping was received, respond with a pong.
                if let ClientMessage::Ping(ping) = msg {
                    server.send_message(
                        client_id,
                        DefaultChannel::ReliableOrdered,
                        bincode::serialize(&ServerMessage::Pong(format!("Pong for ping: '{}'", ping))).unwrap()
                    );
                }
            }
        }

        // Send packets to clients using the transport layer.
        transport.send_packets(&mut server);

        // Without this, my fans went crazy!
        std::thread::sleep(Duration::from_millis(5));
    }
}
