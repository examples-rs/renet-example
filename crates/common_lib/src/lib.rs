use serde::{Serialize, Deserialize};
use renet::transport::NETCODE_USER_DATA_BYTES;

pub const PROTOCOL_ID: u64 = 0; // You change this when you update the program. (To avoid bugs with outdated network code.)

#[derive(Debug, Serialize, Deserialize)]
/// Messages that the client can send.
pub enum ClientMessage {
    Ping(String),
    Marco,
}

#[derive(Debug, Serialize, Deserialize)]
/// Messages that the server can send.
pub enum ServerMessage {
    Pong(String),
    Polo,
}

// Helper struct to pass an username in the user data.
pub struct Username(pub String);

impl Username {
    pub fn to_netcode_user_data(&self) -> Option<[u8; NETCODE_USER_DATA_BYTES]> {
        let mut user_data = [0u8; NETCODE_USER_DATA_BYTES];

        if self.0.len() > NETCODE_USER_DATA_BYTES - 8 {
            // Username is too big!
            return None;
        }

        user_data[0..8].copy_from_slice(&(self.0.len() as u64).to_le_bytes());
        user_data[8..self.0.len() + 8].copy_from_slice(self.0.as_bytes());

        return Some(user_data);
    }

    pub fn from_user_data(user_data: &[u8; NETCODE_USER_DATA_BYTES]) -> Self {
        let mut buffer = [0u8; 8];
        buffer.copy_from_slice(&user_data[0..8]);
        
        let mut len = u64::from_le_bytes(buffer) as usize;
        len = len.min(NETCODE_USER_DATA_BYTES - 8);

        let data = user_data[8..len + 8].to_vec();

        let username = String::from_utf8(data).unwrap();

        return Self(username);
    }
}
