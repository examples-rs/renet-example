# Renet Server Client Example

Renet can be confusing, so here is a hopefully non-confusing example of how it can be used!

If you are looking for an easy to understand Bevy Renet example, check out: [Bevy Renet Example](https://gitlab.com/examples-rs/bevy-renet-example)



# How To Use

To run the server: `cargo run --bin server -- --ip <IP TO LISTEN ON (IP OF THE SERVER)> --max <MAX NUMBER OF CLIENTS ALLOWED TO CONNECT>`

To run the client: `cargo run --bin client -- --server <IP OF THE SERVER>`
